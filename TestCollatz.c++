// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int result = collatz_eval(1, 10);
    ASSERT_EQ(result, 20);
}

TEST(CollatzFixture, eval_2) {
    const int result = collatz_eval(100, 200);
    ASSERT_EQ(result, 125);
}

TEST(CollatzFixture, eval_3) {
    const int result = collatz_eval(201, 210);
    ASSERT_EQ(result, 89);
}

TEST(CollatzFixture, eval_4) {
    const int result = collatz_eval(900, 1000);
    ASSERT_EQ(result, 174);
}

TEST(CollatzFixture, eval_5) {
    const int result = collatz_eval(1, 1);
    ASSERT_EQ(result, 1);
}

TEST(CollatzFixture, eval_6) {
    const int result = collatz_eval(1, 2);
    ASSERT_EQ(result, 2);
}

TEST(CollatzFixture, eval_7) {
    const int result = collatz_eval(4, 4);
    ASSERT_EQ(result, 3);
}

TEST(CollatzFixture, eval_8) {
    const int result = collatz_eval(2, 1);
    ASSERT_EQ(result, 2);
}

TEST(CollatzFixture, eval_9) {
    const int result = collatz_eval(1, 999999);
    ASSERT_EQ(result, 525);
}

TEST(CollatzFixture, eval_10) {
    const int result = collatz_eval(999999, 1);
    ASSERT_EQ(result, 525);
}

TEST(CollatzFixture, eval_11) {
    const int result = collatz_eval(2, 17);
    ASSERT_EQ(result, 20);
}

TEST(CollatzFixture, eval_12) {
    const int result = collatz_eval(17, 2);
    ASSERT_EQ(result, 20);
}

TEST(CollatzFixture, eval_13) {
    const int result = collatz_eval(3, 4);
    ASSERT_EQ(result, 8);
}

TEST(CollatzFixture, eval_14) {
    const int result = collatz_eval(4, 3);
    ASSERT_EQ(result, 8);
}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
